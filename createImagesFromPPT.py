import re
import sys
import io
import os
from reverseImageSearch import ReverseImageSearch
from pptx import Presentation
from PIL import Image

# This program turns the ppt file into a binary stream by using python-pptx and then turns the images found in the stream into actual
# image files, which are stored in the PptImages folder.

print('This script asks for a ppt file, reads it, checks the content for image extensions and then stores them in a txt file.\n')
print('Use the script in the same location as the ppt file exists for ease of use.\n\n')

def getFileName(response):
    try:
        filePattern = '.*(.ppt$|.pptx$)'
        fileName = re.search(filePattern, str(response))
        fileName = fileName.group(0)
    except IOError as e:
        print('Could not read file: '+ 'I/O error({0}): {1}'.format(e.errno, e.strerror))
    except:
        print('You have provided the wrong file format.')
    else:
        if fileName:
            findAndStoreImages(fileName)
        else:
            print('You have provided the wrong file format')

def findAndStoreImages(fileName):
    dir = 'PptImages'
    if not os.path.exists(dir):
        os.makedirs(dir)
    searchSlides(fileName)
    s = ReverseImageSearch(dir)
    s.start()

def searchSlides(fileName):
    file = open(fileName, 'rb')
    ppt = Presentation(file)
    imgCount = 0
    for slide in ppt.slides:
        for shape in slide.shapes:
            if shape.image:
                imgCount +=1
                if isCorrectImageType(shape.image):
                    extension = isCorrectImageType(shape.image)
                    createImageFromBytes(shape.image.blob, shape.image.size, imgCount, extension)
    file.close()
    return 1

def createImageFromBytes(blob, imgSize, imgCount, extension):
    stream = io.BytesIO(blob)
    image = Image.open(stream)

    extension = extension.replace('/', '.')
    image.save('PptImages/'+str(imgCount)+extension)
    #print(str(imgCount) + ' ext: '+extension)

def isCorrectImageType(imageShape):
    imgExtension = imageShape.content_type
    filePattern = '(.jpg$|.jpeg$|.png$|.gif$)'
    image = re.search(filePattern, imgExtension)

    if image.group(0):
        return image.group(0)
    else:
        return 0


py3 = sys.version_info[0] > 2
if py3:
    response = input('please enter the ppt filename: ')
else: 
    response = raw_input('please enter the ppt filename: ')
getFileName(response)
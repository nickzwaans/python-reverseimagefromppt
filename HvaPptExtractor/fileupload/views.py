from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import UploadFileForm
from .createImagesFromPPT import CreateImagesFromFile

# Create your views here.


def index(request):
    context = {'index_text': 'blablabla...'}
    return render(request, 'index.html', context)


def upload_file(request):
    if request.method == 'POST':
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            handle_uploaded_file(request.FILES['file'])
            return HttpResponseRedirect('result.html')
    else:
        form = UploadFileForm()
    return render(request, 'index.html', {'form': form})


def handle_uploaded_file(file):
    filehandler = CreateImagesFromFile
    filehandler.start(file)

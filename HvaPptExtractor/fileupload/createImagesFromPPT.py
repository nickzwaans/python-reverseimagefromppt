import re
import sys
import io
import os
from .reverseImageSearch import ReverseImageSearch
from pptx import Presentation
from PIL import Image

# This program turns the ppt file into a binary stream by using python-pptx and then turns the images found in the stream into actual
# image files, which are stored in the PptImages folder.

class CreateImagesFromFile:

    def start(self, file):
        self.getFileName(file)

    def getFileName(self, file):
        try:
            filePattern = '.*(.ppt$|.pptx$)'
            fileName = re.search(filePattern, str(file))
            fileName = fileName.group(0)
        except IOError as e:
            print('Could not read file: '+ 'I/O error({0}): {1}'.format(e.errno, e.strerror))
        except:
            print('You have provided the wrong file format.')
        else:
            if fileName:
                self.findAndStoreImages(fileName)
            else:
                print('You have provided the wrong file format')

    def findAndStoreImages(self, fileName):
        dir = 'PptImages'
        if not os.path.exists(dir):
            os.makedirs(dir)
        self.searchSlides(fileName)
        s = ReverseImageSearch(dir)
        s.start()

    def searchSlides(self, fileName):
        file = open(fileName, 'rb')
        ppt = Presentation(file)
        imgCount = 0
        for slide in ppt.slides:
            for shape in slide.shapes:
                if shape.image:
                    imgCount +=1
                    if self.isCorrectImageType(shape.image):
                        extension = self.isCorrectImageType(shape.image)
                        self.createImageFromBytes(shape.image.blob, shape.image.size, imgCount, extension)
        file.close()
        return 1

    def createImageFromBytes(self, blob, imgSize, imgCount, extension):
        stream = io.BytesIO(blob)
        image = Image.open(stream)

        extension = extension.replace('/', '.')
        image.save('PptImages/'+str(imgCount)+extension)
        #print(str(imgCount) + ' ext: '+extension)

    def isCorrectImageType(self, imageShape):
        imgExtension = imageShape.content_type
        filePattern = '(.jpg$|.jpeg$|.png$|.gif$)'
        image = re.search(filePattern, imgExtension)

        if image.group(0):
            return image.group(0)
        else:
            return 0
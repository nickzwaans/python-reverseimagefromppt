import requests
from os import listdir


class ReverseImageSearch:

    def __init__(self, imageDirectory):
        self.imageDir = imageDirectory
        self.filePaths = []
        self.urlList = {}

    def start(self):
        self.filePaths = self.getImagesFromDirectory(self.imageDir)
        self.searchUrl = 'https://images.google.com/searchbyimage/upload'
        self.reverseUrls = self.makeRequest(self.filePaths, self.urlList)

        self.makeResultList(self.reverseUrls)

    def getImagesFromDirectory(self, imageDir):
        files = []
        for file in listdir(imageDir):
            files.append(imageDir + '/' + file)
        return files

    def makeRequest(self, filePaths, urlList):
        for filePath in filePaths:
            requestObject = self.createRequestObject(filePath)
            response = requests.post(self.searchUrl, files=requestObject, allow_redirects=False)
            fetchUrl = response.headers['Location']
            self.urlList[filePath] = fetchUrl
        return self.urlList

    def createRequestObject(self, filePath):
        return {'encoded_image': (filePath, open(filePath, 'rb')), 'image_content': ''}

    def makeResultList(self, urlList):
        try:
            file = open('Imagelist.txt', 'w')
        except:
            print('Could not create or open Imagelist.txt')
            return 0
        else:
            file.write('We\'ve found the following images through Google\'s Reverse Image Search:\n')
            for url in urlList:
                file.write(url + ':\n' + urlList[url] + '\n\n')
            file.close()
            return urlList